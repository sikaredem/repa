var express = require('express'),
url = require('url'),
requester = require('./requester');

var server = express();

server.use(express.static(__dirname+'/public'));

server.get('/houses', function(req, res){
  requester.zoopla(req.query, function(error, houses){
    if (error) res.send(500, error.message)
    else {
      res.send(houses);
    }
  });
});

server.listen(80);
