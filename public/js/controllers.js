'use strict';
//var module = angular.module('HousesApp', ['ui'])
function HouseCtrl($scope, $http) {

var map, oldmap, pollutionmap;
function initialize() {
  var mapOptions = {
    zoom: 11,
    center: new google.maps.LatLng(51.4, 0),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
}
 
google.maps.event.addDomListener(window, 'load', initialize);

    $scope.zooplaSubmit = function () {
      $http({url:'http://localhost:80/houses', method:'GET', params: {
          area: $scope.postCode,
          type: $scope.type,
          minimum_price: $scope.minPrice,
          maximum_price: $scope.maxPrice,
          minimum_beds: $scope.minBedrooms
        }}).success(function(data) {
          $scope.houses = data;
          $scope.updateMap();
      });
    };

    $scope.updateMap = function () {
      if ($scope.markers){
        for (var i in $scope.markers){
          if ($scope.markers.hasOwnProperty(i) && $scope.markers[i]){
            $scope.markers[i].setMap(null);
          }
        }
      }
      $scope.markers = [];
      for (var i in $scope.houses){
        if ($scope.houses.hasOwnProperty(i)){
          $scope.markers.push($scope.createMarker(i));
        }   
      }
    }
   
    $scope.createMarker = function (i){
      var house = $scope.houses[i];
      var m = new google.maps.Marker({
        title: house.address,
        map: map, 
        position: new google.maps.LatLng(parseFloat(house.latitude), parseFloat(house.longitude))
      });
      google.maps.event.addListener(m, 'click', function(){
        $scope.$apply($scope.openDialog(i));
      });
      return m;
    }
    
    $scope.showDetails = function(){
        $('#myModal').foundation('reveal', 'open');
    };
    
    $scope.openDialog = function(house_id) {
      console.log(house_id);
      $scope.dialog_house = $scope.houses[house_id];
      $scope.dialog_house.flood_color = getColorClass(true, $scope.dialog_house.flood_risk);
      $scope.dialog_house.green_color = getColorClass(false, $scope.dialog_house.green_percentage);
      $( "#dialog" ).dialog( "open" );
    };

    function getColorClass(smallBetter, percentage) {
      var value = (percentage % 100) / 33 
      if (smallBetter){
        value = (value - 2) * -1;
      }
      return value? ((value - 1)? "success" : "secondary"  ) : "alert"
    }

    $scope.overlayFlood = function(){
        if ($scope.flood) {
            // show the overlay
            console.log("flood on");

        }  else {
            // hide the overlay
            console.log("flood off");
        }
    };
    $scope.overlayLeaf = function(){
        if ($scope.leaf) {
            // show the overlay
            console.log("leaf on");
        }  else {
            // hide the overlay
            console.log("leaf off");
        }
    };
    $scope.overlayCarbon = function(){
        if ($scope.carbon) {
            var imageBounds = new google.maps.LatLngBounds(new google.maps.LatLng(49.691647,-11.888281), new google.maps.LatLng(60.224479,2.860742));
            // show the overlay
            oldmap = new google.maps.GroundOverlay('img/carbon_flux_50percent.png', imageBounds);
            oldmap.setMap(map);
            console.log("carbon on");
        }  else {
            // hide the overlay
            console.log("carbon off");
            oldmap.setMap(null);
        }
    };
    $scope.overlayPollution = function(){
        if ($scope.pollution) {
            var imageBounds = new google.maps.LatLngBounds(new google.maps.LatLng(49.691647,-11.888281), new google.maps.LatLng(60.224479,2.860742));
            // show the overlay
            pollutionmap = new google.maps.GroundOverlay('img/pollution_50percent.png', imageBounds);
            pollutionmap.setMap(map);
            console.log("pollution on");
        }  else {
            // hide the overlay
            console.log("pollution off");
            pollutionmap.setMap(null);
        }
    };

}
