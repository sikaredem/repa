# Installation
### Node
You have to have node and npm installed. Many ways to do this; here's one that I used:
  
    cd
    mkdir repos && cd repos
    git clone https://github.com/joyent/node.git
    cd node
    git checkout v0.10.12 # Or whatever's the current latest version
    mkdir -p ~/opt/node
    ./configure --prefix=~/opt/node # If this fails you're probably using Python 3. You need to use Python 2. You can find out alot bout this on Google.
    make -j 3 # Nr. of cores on your CPU + 1. Makes things a bit faster
    make install
    export PATH=$PATH:~/opt/node/bin
    # path+=~/opt/node/bin # This is for zsh
    echo "export PATH=$PATH:~/opt/node/bin" >> ~/.bashrc # To make the addition to path permanent

### Npm dependencies
The project declares all of it's dependencies in the package.js file. To install those dependencies, you have to run the following in the project's main folder

    npm install

# Running the app
## Normally
To run the app you need to run the following line in the projects main folder:

    node app.js

This will start the app on localhost running on port 80.
## With supervisor
Supervisor is a tool that automatically restarts the server for you when you, for example, change the app.js file.

If you're in the projects main folder, you can run it like this:

    node_modules/.bin/supervisor app.js
   
Or if you add the node_modules/.bin folder to your path:

    supervisor app.js

# Zoopla API
## Form input parameters
* PostCode
* MaxPrice
* MinPrice
* Type
* Minimum Bedrooms
