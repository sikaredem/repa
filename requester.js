var url = require('url'),
http = require('http'),
async = require('async');

module.exports = (function () {

  var zoopla_options = function(query) {
    path ='/' +  url.format({
      pathname: "api/v1/property_listings.js",
      query: {
        area: query.area,
        property_type: query.type,
        maximum_price: query.maximum_price,
        minimum_price: query.minimum_price,
        minimum_beds: query.minimum_beds,
        api_key: "hzkg8umkhjjcq292bz7cdetv"
      }
    });
  
    return {
      hostname: "api.zoopla.co.uk",
      path: path,
      agent: false
    }
  };
  
  var get_zoopla_data = function(query, cb){ 
    http.get(zoopla_options(query), function (res){
      res.setEncoding('utf8');
      var str = ""
      res.on('data', function(chunk) {
        str += chunk;
      });
      res.on('end', function(){
        var result = JSON.parse(str);
        var length = result.listing.length
        var element = null;
        var houses = [];
        for (var i = 0; i < length; i++) {
          element = result.listing[i];
          houses.push({
            latitude: element.latitude,
            longitude: element.longitude,
            num_bedrooms: element.num_bedrooms,
            address: element.displayable_address,
            price: element.price,
            thumbnail_url: element.thumbnail_url,
            image_url: element.image_url,
            flood_risk: 24,
            green_percentage: 38
          });
        }
	cb(null, JSON.stringify(houses));
      });
      res.on('error', function(e) {
        cb(new Error(e.message));
      });
    });
  }

  return {
    zoopla: get_zoopla_data
  }


})();
